package com.df.storage;

/**
 * Vector is a list, backed by an array and synchronized.
 * All optional operations including adding, removing, and replacing elements are supported.
 * <p>
 * <p>All elements are permitted, including null.
 * <p>
 * <p>
 * Created by dongfang on 2016/3/21.
 */
public class Vector {
}
