package com.df.storage;

/**
 * @author dongfang
 */
public class Node {

    public Node left;
    public Node right;
    public int value;

    public Node(int v) {
        this.value = v;
        left = null;
        right = null;
    }
}
