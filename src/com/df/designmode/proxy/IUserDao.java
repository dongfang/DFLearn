package com.df.designmode.proxy;

/**
 * @author dongfang
 */
public interface IUserDao {
    void save();
}
