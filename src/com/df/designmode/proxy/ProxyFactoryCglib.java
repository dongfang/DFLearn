package com.df.designmode.proxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author dongfang
 */
public class ProxyFactoryCglib implements MethodInterceptor {

    private Object target;

    public ProxyFactoryCglib(Object object) {
        this.target = object;
    }


    public Object newProxyInstance() {
        Enhancer en = new Enhancer();
        en.setSuperclass(target.getClass());
        en.setCallback(this);
        return en.create();
    }


    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("ProxyFactoryCglib#intercept 0");

        Object result = method.invoke(target, objects);

        System.out.println("ProxyFactoryCglib#intercept 1");

        return result;
    }
}
