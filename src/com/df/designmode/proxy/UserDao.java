package com.df.designmode.proxy;

/**
 * @author dongfang
 */
public class UserDao implements IUserDao {
    @Override
    public void save() {
        System.out.println("UserDao#save");
    }
}
