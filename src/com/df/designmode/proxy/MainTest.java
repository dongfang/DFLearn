package com.df.designmode.proxy;

/**
 * @author dongfang
 */
public class MainTest {


    public static void main(String[] args) {
        UserDao target = new UserDao();
        System.out.println(target.getClass());

        UserDao proxy = (UserDao) new ProxyFactoryCglib(target).newProxyInstance();
        System.out.println(proxy.getClass());

        target.save();
        proxy.save();

    }
}
