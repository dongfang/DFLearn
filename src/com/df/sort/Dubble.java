package com.df.sort;

/**
 * 冒泡
 *
 * @author dongfang
 */
public class Dubble {
    /**
     * <EM>冒泡排序（Bubble Sort，台湾译为：泡沫排序或气泡排序）</EM>是一种简单的排序算法。<BR/>
     * 它重复地走访过要排序的数列，一次比较两个元素，如果他们的顺序错误就把他们交换过来。
     * 走访数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成。
     * 这个算法的名字由来是因为越小的元素会经由交换慢慢“浮”到数列的顶端。
     * <p>
     * 步骤：
     * <pre>
     * 1.比较相邻的元素。如果第一个比第二个大，就交换他们两个。
     * 2.对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对。在这一点，最后的元素应该会是最大的数。
     * 3.针对所有的元素重复以上的步骤，除了最后一个。
     * 4.持续每次对越来越少的元素重复上面的步骤，直到没有任何一对数字需要比较。
     * <pre/>
     *
     * @param array
     */
    public static void dubble(int[] array) {
        int num = 1;
        int length = array.length;
        int temp = 0;
        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;

                    num++;
                    // System.out.println("[" + num++ + "][" + i + "][" + j + "] : " + Arrays.toString(array));
                }
            }
        }

        System.out.println("dubble num [" + num + "]");
    }


    /**
     * 冒泡排序。
     * 若当前为第i次循环；
     * 则i与大于i的元素一次比较，使得i位置上变成最小元素；
     *
     * @param array
     */
    public static void dubbleAlpha(int[] array) {
        int num = 1;
        int length = array.length;
        int temp = 0;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (array[i] > array[j]) {
                    temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                    num++;
                    // System.out.println("[" + num++ + "][" + i + "][" + j + "] : " + Arrays.toString(array));
                }
            }
        }
        System.out.println("dubbleAlpha num [" + num + "]");

    }
}
