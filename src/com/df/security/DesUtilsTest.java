package com.df.security;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DesUtilsTest {

    @org.junit.jupiter.api.Test
    void getInstance() throws Exception {
        DesUtils desUtils = new DesUtils("1564543227987");
        String str1 = desUtils.decrypt("5d238f86fd0734ea938abcf6d8775e48");
        System.out.println("str1[" + str1 +"]");

    }

    public void setSessionTime() {
        try {
            String str1 = (new DesUtils("1564479644191")).decrypt("3c7319930aae1afe8de9dd5b178e888ec684ed86da99dd88d8a84f84aed3158ad1efd4c25c3822b9e433e39410e181297c4830c2f009a0cd16c4b59915fd398e95422844508717e0c40ea932eaea5a103fe446c69ac90b692669bfdba576581cdb3737a1771c743e6f539fa6fa33a1ec");
            System.out.println("str1[" + str1 +"]");
//            DesUtils.getInstance().setKey(str);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    @org.junit.jupiter.api.Test
    void byteArr2HexStr() throws Exception {
        DesUtils desUtils = DesUtils.getInstance();
        byte[] paramArr = "111".getBytes();

        String str = DesUtils.byteArr2HexStr(paramArr);
        System.out.println(str);

        byte[] paramArray = DesUtils.hexStr2ByteArr("313131");
        System.out.println(new String(paramArray));
    }

    @org.junit.jupiter.api.Test
    void hexStr2ByteArr() {
    }

    @Test
    void decrypt() {
        DesUtils desUtils = DesUtils.getInstance();
        try {
//            System.out.println(desUtils.decrypt("71b781fe85a23834"));
//            System.out.println(desUtils.decrypt("2d44e313de33f28d"));
            System.out.println(desUtils.decrypt("1af9dd2e85174164bd1b217ffb128bb9"));
            desUtils.setKey("sangzhonghai");
            System.out.println(desUtils.decrypt("ba26759323af2eece2ec37222f5bffa7"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void encrypt() {
        DesUtils desUtils = DesUtils.getInstance();
        try {
            desUtils.setKey("sangzhonghai");
            System.out.println("prepare() : " + desUtils.encrypt("prepare()"));
            System.out.println("reset() : " + desUtils.encrypt("reset()"));
            System.out.println("start() : " + desUtils.encrypt("start()"));
            System.out.println("release() : " + desUtils.encrypt("release()"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}