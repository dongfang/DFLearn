package com.df.security;

import java.security.Key;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class DesUtils {
    private static DesUtils instance;
    private static String strDefaultKey = "woaichibaicai";
    private Cipher decryptCipher = null;
    private Cipher encryptCipher = null;

    public DesUtils() throws Exception {
        this(strDefaultKey);
    }

    public DesUtils(String paramString) throws Exception {
        Key key = getKey(paramString.getBytes());
        this.encryptCipher = Cipher.getInstance("DES");
        this.encryptCipher.init(Cipher.ENCRYPT_MODE, key);
        this.decryptCipher = Cipher.getInstance("DES");
        this.decryptCipher.init(Cipher.DECRYPT_MODE, key);
    }

    public static synchronized DesUtils getInstance() {
        if (instance == null) {
            try {
                instance = new DesUtils();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return instance;
    }

    private Key getKey(byte[] paramArrayOfByte) throws Exception {
        byte[] arrayOfByte = new byte[8];
        for (int b = 0; b < paramArrayOfByte.length && b < arrayOfByte.length; b++) {
            arrayOfByte[b] = paramArrayOfByte[b];
        }
        return new SecretKeySpec(arrayOfByte, "DES");
    }


//    public static String byteArr2HexStr(byte[] paramArrayOfByte) throws Exception {
//        int i = paramArrayOfByte.length;
//        StringBuffer stringBuffer = new StringBuffer(i * 2);
//        byte b = 0;
//        label14: while (true) {
//            if (b >= i)
//                return stringBuffer.toString();
//            for (byte b1 = paramArrayOfByte[b];; b1 += 256) {
//                if (b1 >= 0) {
//                    if (b1 < 16)
//                        stringBuffer.append("0");
//                    stringBuffer.append(Integer.toString(b1, 16));
//                    b++;
//                    continue label14;
//                }
//            }
//        }
//    }

    public static String byteArr2HexStr(byte[] byteArray) {
        final StringBuilder hexString = new StringBuilder("");
        if (byteArray == null || byteArray.length <= 0)
            return null;
        for (byte b : byteArray) {
            int v = b & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                hexString.append(0);
            }
            hexString.append(hv);
        }
        return hexString.toString();
    }


    public static byte[] hexStr2ByteArr(String paramString) throws Exception {
        byte[] arrayOfByte1 = paramString.getBytes();
        byte[] arrayOfByte2 = new byte[arrayOfByte1.length / 2];
        for (int b = 0; b < arrayOfByte1.length; b += 2) {
            String str = new String(arrayOfByte1, b, 2);
            arrayOfByte2[b / 2] = (byte) Integer.parseInt(str, 16);
        }
        return arrayOfByte2;
    }

    public String decrypt(String paramString) throws Exception {
        return new String(decrypt(hexStr2ByteArr(paramString)));
    }

    public byte[] decrypt(byte[] paramArrayOfByte) throws Exception {
        return this.decryptCipher.doFinal(paramArrayOfByte);
    }

    public String decryptString(String paramString) throws Exception {
        String str = paramString;
        if (paramString != null)
            try {
                return getInstance().decrypt(paramString);
            } catch (Exception exception) {
                exception.printStackTrace();
                return str;
            }
        return str;
    }

    public String encrypt(String paramString) throws Exception {
        return byteArr2HexStr(encrypt(paramString.getBytes()));
    }

    public byte[] encrypt(byte[] paramArrayOfByte) throws Exception {
        byte[] bytes = this.encryptCipher.doFinal(paramArrayOfByte);
        return bytes;
    }

    public String encryptKeyString() {
        String str = "";
        if (strDefaultKey != null)
            try {
                return encrypt(strDefaultKey);
            } catch (Exception exception) {
                exception.printStackTrace();
                return str;
            }
        return str;
    }

    public String encryptString(String paramString) throws Exception {
        String str = paramString;
        if (paramString != null)
            try {
                return getInstance().encrypt(paramString);
            } catch (Exception exception) {
                exception.printStackTrace();
                return str;
            }
        return str;
    }

    public void setKey(String paramString) throws Exception {
        try {
            Key key = getKey(paramString.getBytes());
            this.encryptCipher.init(1, key);
            this.decryptCipher.init(2, key);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
