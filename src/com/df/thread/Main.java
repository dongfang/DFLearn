package com.df.thread;

/**
 * Auth dongfang
 * Date 2019-06-23
 */
public class Main {

    public volatile static int type = 0;

    public static void main(String[] args) {
        Object a = new Object();
        new R(0, a).start();
        new R(1, a).start();


    }


    static class R extends Thread {

        int t = -1;
        Object lock;

        public R(int type, Object o) {
            this.t = type;
            this.lock = o;
        }

        @Override
        public void run() {
            synchronized (lock) {
                while (true) {
                    try {
                        System.out.print(t);
                        lock.notifyAll();
                        sleep(100);
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
