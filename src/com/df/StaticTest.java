package com.df;

/**
 * @author dongfang
 */
public class StaticTest {
    static {
        // 在第一次类被加载（初始化或调用）的时候，会运行，不过只会运行一次
        System.out.println("class StaticTest # static block # test");
    }

    public StaticTest() {
        System.out.println("construct");
    }


    public static void staticFunction() {
        System.out.println("class StaticTest # staticFunction ");

    }
}
